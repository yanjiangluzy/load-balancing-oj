#include "complie_run.hpp"
#include "../comm/httplib.h"
using namespace Run_Complie;
using namespace httplib;


void usage()
{
    std::cout << "./xxx [-port]" << std::endl;
}
int main(int argc, char* argv[])
{
    //为了实现负载均衡调度，需要在不同的端口号上部署该服务以供oj_server模块分配
    if (argc != 2)
    {
        usage();
        exit(1);
    }
    
    Server svr;


    svr.Post("/complie_and_run", [](const Request &req, Response &resp){
        // 用户请求的服务正文是我们想要的json string
        std::string in_json = req.body;
        std::string out_json;
        if(!in_json.empty()){
            run_complie::start(in_json, &out_json);
            resp.set_content(out_json, "application/json;charset=utf-8");
        }
    });

    svr.listen("0.0.0.0", atoi(argv[1])); //启动http服务
    
    return 0;
}