var user_id = -1;
var admistrator_id = -1;
var user_name = "";
var admi_name = "";

function CheckUser() {
    console.log(user_id);
    if (user_id > 0) {
        $(".nav_bar .last_li").text(user_name);
        return;
    }
    $.ajax({
        url: "/GetUserId",
        method: "Get",
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        //async : false ==> 禁止异步请求   chrome 有可能会禁止 ajax 的同步请求
        //async: false,
        success: function (data) {
            console.log(data);
            if (data.id > 0) {
                user_id = data.id;
                GetUsername();
                console.log(user_name);
                $(".nav_bar .last_li").text(user_name);
                // $(".nav_bar .last_li").attr("href", "#");
            } else {
                alert("请先进行登录");
                window.location.href = "/sign in.html";
            }
        },
    });
}

function CheckAdministor() {
    console.log(admistrator_id);
    if (admistrator_id > 0) {
        $(".nav_bar .last_li").text(admi_name);
        $(".nav_bar .last_li").attr("href", "#");
        return;
    }
    $.ajax({
        url: "/GetUserId",
        method: "Get",
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        //async : false ==> 禁止异步请求   chrome 有可能会禁止 ajax 的同步请求
        //async: false,
        success: function (data) {
            console.log(data);
            if (data.id > 0) {
                admistrator_id = data.id;
                GetAdinimistorname();
                $(".nav_bar .last_li").text(admi_name);
                $(".nav_bar .last_li").attr("href", "#");
            } else {
                alert("请先进行登录");
                window.location.href = "/administrator.html";
            }
        },
    });
}

function GetUsername() {
    $.ajax({
        url: "/GetUserName",
        method: "Post",
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        //async : false ==> 禁止异步请求   chrome 有可能会禁止 ajax 的同步请求
        async: false,
        data: JSON.stringify({
            'id': user_id,
            'strId': "user_id",
            'table': "user"
        }),

        success: function (data) {
            console.log(data);
            user_name = data.username;
            console.log(user_name);
        },
    });
}

function GetAdinimistorname() {
    $.ajax({
        url: "/GetUserName",
        method: "Post",
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        //async : false ==> 禁止异步请求   chrome 有可能会禁止 ajax 的同步请求
        async: false,
        data: JSON.stringify({
            'id': admistrator_id,
            'strId': "admistrator_id",
            'table': "administrators"
        }),
        success: function (data) {
            admi_name = data.username;
            console.log(admi_name);
        },
    });
}

