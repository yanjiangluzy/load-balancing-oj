#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    pid_t id = fork();
    if (id < 0)
        return 0;

    else if (id == 0)
    {
        //
        execl("./temp/[2023-05-27:17:41:05].exe", "./temp/[2023-05-27:17:41:05].exe", nullptr);
        exit(10);
    }
    else
    {
        waitpid(id, nullptr, 0);
    }
    return 0;
}