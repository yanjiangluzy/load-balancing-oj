#pragma once
#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
void MyDaemon()
{
    // 0.忽略一些信号
    signal(SIGCHLD, SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    // 1.保证自己不是组长
    if (fork() > 0)
        exit(1);
    // 2.重定向
    int devnull = open("/dev/null", O_RDONLY | O_WRONLY);
    dup2(0, devnull);
    dup2(1, devnull);
    dup2(2, devnull);
    close(devnull);
}