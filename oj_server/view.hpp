#pragma once
#include <iostream>
#include "model_db.hpp"
#include <ctemplate/template.h>

const std::string template_path = "./template_html/"; // 设定

class View
{
public:
    View() {}
    std::string expandAllQuestions(const std::vector<Question> &questions)
    {
        std::cout << "view模块, 开始渲染题目列表" << std::endl;
        // 返回渲染后的所有题目列表的html网页
        // 1. 定义字典,这里的字典需要嵌套（因为有多道题目）
        ctemplate::TemplateDictionary root("all_question");

        for (const auto &q : questions)
        {
            ctemplate::TemplateDictionary *sub = root.AddSectionDictionary("question_list"); // 这是待会html中进行循环的列表
            // 插入kv值
            sub->SetValue("number", std::to_string(q.number));
            sub->SetValue("name", q.name);
            sub->SetValue("level", q.level);
        }

        // 2.获取被渲染对象
        ctemplate::Template *tpl = ctemplate::Template::GetTemplate(template_path + "all_questions.html", ctemplate::DO_NOT_STRIP);

        // 3.开始完成渲染
        std::string res_html; // 返回的html网页
        tpl->Expand(&res_html, &root);

        return res_html;
    }
    std::string expandOneQuestion(const Question &q)
    {
        // 返回渲染后的某一道题目
        // 需要题号，题目名字，难度，题目的详细描述, 预置代码等
        ctemplate::TemplateDictionary root("one_question");
        root.SetValue("number", std::to_string(q.number));
        root.SetValue("name", q.name);
        root.SetValue("level", q.level);
        root.SetValue("description", q.description);
        root.SetValue("body", q.body);

        // 获取被渲染对象
        ctemplate::Template *tpl = ctemplate::Template::GetTemplate(template_path + "one_question.html", ctemplate::DO_NOT_STRIP);
        std::string res_html; // 返回的网页

        tpl->Expand(&res_html, &root);
        return res_html;
    }

    std::string expandSolve(const Solve &s)
    {
        // 1. 定义字典
        ctemplate::TemplateDictionary root("slove");

        // 2. 给出数据
        root.SetValue("number", std::to_string(s.number));
        root.SetValue("solve", s.solve);
        root.SetValue("code", s.solve_code);
        root.SetValue("author", s.author);
        root.SetValue("title", s.title);

        // 3. 获取被渲染对象
        ctemplate::Template *tpl = ctemplate::Template::GetTemplate(template_path + "solve.html", ctemplate::DO_NOT_STRIP);
        std::string res_html; // 返回的网页

        // 4. 开始渲染
        tpl->Expand(&res_html, &root);

        return res_html;
    }

    // 渲染论坛网页
    std::string expandDiscussions(const std::vector<Discussion> &discussions)
    {
        std::cout << "view模块, 开始渲染帖子列表" << std::endl;
        // 返回渲染后的所有题目列表的html网页
        // 1. 定义字典,这里的字典需要嵌套（因为有多道题目）
        ctemplate::TemplateDictionary root("all_discussions");

        for (const auto &s : discussions)
        {
            ctemplate::TemplateDictionary *sub = root.AddSectionDictionary("discussions_list"); // 这是待会html中进行循环的列表
            // 插入kv值
            sub->SetValue("id", std::to_string(s.id));
            sub->SetValue("author", s.author);
            sub->SetValue("title", s.title);
            sub->SetValue("desc", s.desc);

        }

        // 2.获取被渲染对象
        ctemplate::Template *tpl = ctemplate::Template::GetTemplate(template_path + "discussions.html", ctemplate::DO_NOT_STRIP);

        // 3.开始完成渲染
        std::string res_html; // 返回的html网页
        tpl->Expand(&res_html, &root);

        return res_html;
    }

    // 渲染文章网页
    std::string expandDiscussion(Discussion &s)
    {
        // 1. 定义字典
        ctemplate::TemplateDictionary root("article");

        // 2. 给出数据
        root.SetValue("id", std::to_string(s.id));
        root.SetValue("maintext", s.maintext);
        root.SetValue("author", s.author);
        root.SetValue("title", s.title);

        // 3. 获取被渲染对象
        ctemplate::Template *tpl = ctemplate::Template::GetTemplate(template_path + "article.html", ctemplate::DO_NOT_STRIP);
        std::string res_html; // 返回的网页

        // 4. 开始渲染
        tpl->Expand(&res_html, &root);

        return res_html;
    }

    ~View() {}
};