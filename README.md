# 负载均衡oj

## 介绍
实现leetcode中的题库功能，使用负载均衡算法提高题目提交后后台编译判断的效率

## 项目架构
项目分成三个模块
comm: 公共模块
complie_server: 编译运行
oj_server: 获取题目列表，实现负载均衡分配

## 各层次介绍
文件结构

1. comm
> comm模块主要存放其他模块   
2. complie_server
3. oj_server

