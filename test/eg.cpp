#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;
class Solution
{
public:
    int maxValue(const vector<int> &nums)
    {
        // write your code there
        //...
        return 6;
    }
};
#ifndef COMPILER_ONLINE
#include "head.cpp"
#endif

void Test1()
{
    vector<int> num1 = {1, 3, 5, 6, 2, 4};
    int ret = Solution().maxValue(num1);
    if (ret == 6)
    {
        std::cout << "用例1通过" << std::endl;
    }
}

void Test2()
{
    vector<int> num2 = {4, 1, 6, 7, 10};
    int ret = Solution().maxValue(num2);
    if (ret == 10)
    {
        std::cout << "用例2通过" << std::endl;
    }
}
int main()
{
    std::cout << "t" << std::endl;
    Test1();
    Test2();
    return 0;
}
