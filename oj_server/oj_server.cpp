#include <iostream>
#include "control.hpp"
#include "../comm/httplib.h"
#include "MyDaemon.hpp"
using namespace httplib;

static Control *ctrl_ptr = nullptr;
const std::string suffix_path = "./template_html/";

std::string readHtml(const std::string path)
{
    // 获取502网页
    std::ifstream in(path);
    if (!in.is_open())
    {
        logMessage(FATAL, "%s, [%s,%d]", "打开文件失败!!!", __FILE__, __LINE__);
        return "";
    }
    std::string res;
    std::string line;
    while (std::getline(in, line))
    {
        res += line;
        // 可能需要加上\n TODO
    }

    return res;
}

int main()
{

    Control ctrl;
    MyDaemon();

    // 目前总共提供三个功能：1.题目列表，2.获取题目详细内容，3.判题功能
    // 提供服务路由,使用正则表达式匹配url
    Server svr;
    svr.Get(R"(/hello)", [](const Request &req, Response &res)
            { res.set_content("你好", "text/plain;charset=utf-8"); });

    // //1. 获取每一道题目
    svr.Get(R"(/question/(\d+))", [&ctrl](const Request &req, Response &res)
            {
        std::cout << "开始获取指定题目" << std::endl;
        //题号就是matches的第二个元素
        std::string number = req.matches[1];
        std::string html = ctrl.getOneQuestionOfHtml(atoi(number.c_str()));
        if (html == "") 
        {
            html = readHtml(suffix_path + "502.html");
            if (html == "") html += "获取题目列表失败";
        }
        res.set_content(html, "text/html;charset=utf-8"); });

    // 2.获取所有题目列表
    svr.Get("/all_questions", [&ctrl](const Request &req, Response &res)
            {
        std::cout << "开始获取所有题目" << std::endl;

        std::string html =ctrl.getALLQuestionsOfHtml();
        if (html == "") 
        {
            html = readHtml(suffix_path + "502.html");
            if (html == "") html += "获取题目列表失败";  // 在这里可以制定返回出错的页面
        }
        res.set_content(html, "text/html;charset=utf-8"); });

    // 3.获取判题功能
    svr.Post(R"(/judge/(\d+))", [&ctrl](const Request &req, Response &res)
             {
        std::string number = req.matches[1];
        std::string result_json;
        std::cout << "开始判题" << std::endl;
        ctrl.judge(number, req.body, &result_json);
        std::cout << "结束判题" << std::endl;
        res.set_content(result_json, "application/json;charset=utf-8"); });

    // // 4.获取登录界面
    svr.Get("/login", [&ctrl](const Request &req, Response &res)
            {
        std::string html = readHtml(suffix_path + "login.html");
        if (html == "") 
        {
            html = readHtml(suffix_path + "502.html");
            if (html == "") html += "获取题目列表失败";  // 在这里可以制定返回出错的页面
        }
        res.set_content(html, "text/html;charset=utf-8"); });

    // 5. 获取注册界面
    svr.Get("/register", [&ctrl](const Request &req, Response &res)
            {
        std::string html = readHtml(suffix_path + "register.html");
        if (html == "") 
        {
            html = readHtml(suffix_path + "502.html");
            if (html == "") html += "获取题目列表失败";  // 在这里可以制定返回出错的页面
        }
        res.set_content(html, "text/html;charset=utf-8"); });

    // 6. 登录
    svr.Post("/login", [&ctrl](const Request &req, Response &res)
             {
        std::cout << "登录验证开始..." << std::endl;
        std::string result_json;
        ctrl.toLogin(req.body, &result_json);

        res.set_content(result_json, "application/json;charset=utf-8"); });

    // 7. 注册
    svr.Post("/register", [&ctrl](const Request &req, Response &res)
             {
        std::cout << "注册开始..." << std::endl;
        std::string result_json;
        ctrl.toRegister(req.body, &result_json);

        res.set_content(result_json, "application/json;charset=utf-8"); });

    // 8. 获取题解
    svr.Get(R"(/solve/(\d+))", [&ctrl](const Request &req, Response &res)
            {
    std::cout << "开始获取指定题解" << std::endl;
    //题号就是matches的第二个元素
    std::string number = req.matches[1];
    std::string html = ctrl.getSolve(atoi(number.c_str()));
    if (html == "")
    {
        html = readHtml(suffix_path + "502.html");
        if (html == "") html+= "网站正在维护，请稍后访问...";
    }
    res.set_content(html, "text/html;charset=utf-8"); });

    // 9.获取帖子界面
    svr.Get("/discussion", [&ctrl](const Request &req, Response &res)
            {
        std::string html = ctrl.getDiscussions();
        if (html == "")
            html += readHtml(suffix_path + "502.html");
        res.set_content(html, "text/html;charset=utf-8"); });

    // 10.获取特定一篇文章
    svr.Get(R"(/article/(\d+))", [&ctrl](const Request &req, Response &res)
            {
    std::cout << "开始获取指定文章" << std::endl;
    //文章编号就是matches的第二个元素
    std::string number = req.matches[1];
    std::string html = ctrl.getDiscussion(atoi(number.c_str()));
    if (html == "")
    {
        html = readHtml(suffix_path + "502.html");
        if (html == "") html+= "网站正在维护，请稍后访问...";
    }
    res.set_content(html, "text/html;charset=utf-8"); });

    // 11.手动录题
    // error page
    svr.Get("/error", [&ctrl](const Request &req, Response &res)
            {
        std::string html = readHtml(suffix_path + "502.html");
        res.set_content(html, "text/html;charset=utf-8"); });

    // 13. 获取发帖界面
    svr.Get("/post-article", [&ctrl](const Request &req, Response &res)
            {
        std::cout << "开始获取发帖界面" << std::endl;
        std::string html = readHtml(suffix_path + "post_article.html");
        if (html == "")
            html += readHtml(suffix_path + "502.html");
        res.set_content(html, "text/html;charset=utf-8"); });
    
    // 14. 接收发帖的数据
    svr.Post("/post-article", [&ctrl](const Request &req, Response &res)
             {
        std::cout << "开始接收帖子数据..." << std::endl;
        std::string result_json;
        ctrl.toPostArticle(req.body, &result_json);

        res.set_content(result_json, "application/json;charset=utf-8"); });

    // 15. 关于我们
    svr.Get("/about", [&ctrl](const Request &req, Response &res)
            {
        std::cout << "开始获取发帖界面" << std::endl;
        std::string html = readHtml(suffix_path + "about.html");
        if (html == "")
            html += readHtml(suffix_path + "502.html");
        res.set_content(html, "text/html;charset=utf-8"); });

    // 16. 联系我们
    svr.Get("/call", [&ctrl](const Request &req, Response &res)
            {
        std::cout << "开始获取发帖界面" << std::endl;
        std::string html = readHtml(suffix_path + "call.html");
        if (html == "")
            html += readHtml(suffix_path + "502.html");
        res.set_content(html, "text/html;charset=utf-8"); });

        
    // 设置首页
    svr.set_base_dir("./MyRoot");

    // 监听服务
    svr.listen("0.0.0.0", 8081);
    return 0;
}
