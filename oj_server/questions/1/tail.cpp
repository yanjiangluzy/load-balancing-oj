#ifndef COMPILER_ONLINE
#include "head.hpp"
#endif

// 定义正确的解法
int findMaxNum(const vector<int> &nums)
{
    int res = INT_MIN;
    for (auto &num : nums)
        res = res > num ? res : num;
    return res;
}

// 这个文件包含测试代码
bool test(const vector<int>& nums)
{
    return (Solution().maxValue(nums) == findMaxNum(nums));
}




int main()
{
    // 测试数据
    vector<vector<int>> test_datas = {
        {1, 2, 3, 4, 5},
        {2, 3, 4, 5, 6},
        {-1, -3, 5, 7, 8},
        {}
    };

    int cnt = 1;
    for (auto &test_data : test_datas)
    {
        if (!test(test_data))
        {
            cout << "测试用例" << cnt << "未通过" << endl;
            printf("正确答案: %d\n 你的输出: %d\n", findMaxNum(test_data), Solution().maxValue(test_data));
            return 0;
        } 
    }

    cout << "通过全部测试用例" << endl;
    return 0;
}
