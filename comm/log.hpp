#pragma once
#include <iostream>
#include <stdarg.h>
#include <ctime>
#include <string>
#include <cstring>
#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

const char *gLevelMap[] = {
    "DEBUG",
    "NORMAL",
    "WARNING",
    "ERROR",
    "FATAL"
};

// 方便打印行号
std::string printLF = "[%s:%d]";

namespace Log
{
    void logMessage(int level, const char *format, ...)
    {
        // 打印标准部分
        char stdBuffer[1024];
        time_t timestamp = time(nullptr);
        struct tm *mytime = gmtime(&timestamp);
        mytime->tm_hour += 8; // 特殊情况处理
        std::string timeFormat = "[%Y-%m-%d:%H:%M:%S]";
        char timeBuffer[50];
        strftime(timeBuffer, sizeof(timeBuffer), timeFormat.c_str(), mytime);
        snprintf(stdBuffer, sizeof(stdBuffer) - 1, "[%s]: ", gLevelMap[level]);
        strcat(stdBuffer, timeBuffer);
        // 打印可变参数
        char vaBuffer[1024];
        va_list arg; // 定义了一个指针
        va_start(arg, format);
        vsnprintf(vaBuffer, sizeof(vaBuffer) - 1, format, arg); // 特殊函数
        va_end(arg);
        printf("%s: %s\n", stdBuffer, vaBuffer);
    }
}
